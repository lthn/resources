#!/usr/bin/env bash
SERVER='https://gitlab.com/'

mkdir -p lthn.io/projects/chain

git clone ${SERVER}lthn.io/projects/chain/lethean.git lthn.io/projects/chain/lethean
git clone ${SERVER}lthn.io/projects/chain/miner.git lthn.io/projects/chain/miner
git clone ${SERVER}lthn.io/projects/chain/wallet.git lthn.io/projects/chain/wallet

mkdir -p lthn.io/projects/sdk

git clone ${SERVER}lthn.io/projects/sdk/nodejs.git lthn.io/projects/sdk/nodejs
git clone ${SERVER}lthn.io/projects/sdk/build.git lthn.io/projects/sdk/build
git clone ${SERVER}lthn.io/projects/sdk/shell.git lthn.io/projects/sdk/shell

mkdir -p lthn.io/projects/vpn/extensions

git clone ${SERVER}lthn.io/projects/vpn/extensions/chrome-browser.git lthn.io/projects/vpn/extensions/chrome-browser
git clone ${SERVER}lthn.io/projects/vpn/extensions/firefox-browser.git lthn.io/projects/vpn/extensions/firefox-browser

git clone ${SERVER}lthn.io/projects/vpn/node.git lthn.io/projects/vpn/node
